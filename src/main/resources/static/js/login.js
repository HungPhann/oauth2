$(document).ready(function () {

    $("body").on("click", "#login_button", function () {
        var data = {
            email: $("#email").val(),
            password: $("#password").val(),
            client_id: $("#client_id").val(),
            redirect_uri: $("#redirect_uri").val(),
            scope: $("#scope").val(),
            state: $("#state").val(),
            response_type: $("#response_type").val()
        };

        $.ajax({
            url: "login",
            contentType: "application/json; charset=UTF-8",
            type: "post",
            data: JSON.stringify(data),
            success: function (response) {
                window.location = response["redirect_uri"];
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert("Invalid email and password!");
            }
        });
    })


});