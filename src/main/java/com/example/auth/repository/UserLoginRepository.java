package com.example.auth.repository;

import com.example.auth.model.UserLogin;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface UserLoginRepository extends CrudRepository<UserLogin, Long> {
    Optional<UserLogin> findByEmail(String email);

    Optional<UserLogin> findByUsernameOrEmail(String username, String email);

    List<UserLogin> findByIdIn(List<Long> userIds);

    Optional<UserLogin> findByUsername(String username);

    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);
}
