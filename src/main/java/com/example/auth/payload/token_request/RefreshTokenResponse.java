package com.example.auth.payload.token_request;

import com.example.auth.security.helper.JWTConstant;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

public class RefreshTokenResponse extends TokenResponse{
    private String token_type = JWTConstant.tokenType;
    private String access_token;
    private long expires_in;

    public RefreshTokenResponse(String access_token, long expires_in){
        this.token_type = token_type;
        this.access_token = access_token;
        this.expires_in = expires_in;
    }

    public String getToken_type() {
        return token_type;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public void setExpires_in(long expires_in) {
        this.expires_in = expires_in;
    }

    public long getExpires_in() {
        return expires_in;
    }
}
