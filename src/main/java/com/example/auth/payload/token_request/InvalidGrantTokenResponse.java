package com.example.auth.payload.token_request;

import com.example.auth.payload.token_request.TokenResponse;

public class InvalidGrantTokenResponse extends TokenResponse {
    private final String error = "invalid_grant";

    public String getError() {
        return error;
    }
}
