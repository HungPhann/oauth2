package com.example.auth.payload.login_request;

import javax.validation.constraints.NotBlank;

public class LoginRequest {
    @NotBlank
    private String email;

    @NotBlank
    private String password;


    private String client_id;


    private String redirect_uri;

    private String scope;


    private String state;


    private String response_type;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getClient_id() {
        return client_id;
    }

    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }

    public String getRedirect_uri() {
        return redirect_uri;
    }

    public void setRedirect_uri(String redirect_uri) {
        this.redirect_uri = redirect_uri;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getResponse_type() {
        return response_type;
    }

    public void setResponse_type(String response_type) {
        this.response_type = response_type;
    }

    @Override
    public String toString() {
        String result = "email: " + email + "\n";
        result += "password: " + password + "\n";
        result += "clientID: " + client_id + "\n";
        result += "redirect_uri: " + redirect_uri + "\n";
        result += "scope: " + scope + "\n";
        result += "state: " + state + "\n";
        result += "response_type: " + response_type + "\n";
        return result;
    }
}