package com.example.auth.payload.login_request;

public class LoginResponse {

    public boolean success;

    private String redirect_uri;

    public LoginResponse(boolean success, String redirect_uri){
        this.success = success;
        this.redirect_uri = redirect_uri;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getRedirect_uri() {
        return redirect_uri;
    }

    public void setRedirect_uri(String redirect_uri) {
        this.redirect_uri = redirect_uri;
    }
}