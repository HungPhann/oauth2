package com.example.auth.security.helper;

import com.example.auth.payload.token_request.AccessTokenResponse;
import com.example.auth.payload.token_request.InvalidGrantTokenResponse;
import com.example.auth.payload.token_request.RefreshTokenResponse;
import com.example.auth.payload.token_request.TokenResponse;
import com.example.auth.security.UserPrincipal;
import io.jsonwebtoken.*;
import io.jsonwebtoken.impl.crypto.MacProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.security.Key;
import java.util.Base64;
import java.util.Date;

@Component
public class JwtTokenProvider {

    private static final Logger logger = LoggerFactory.getLogger(JwtTokenProvider.class);

    private static final String secret = JWTConstant.secret;
    //private static final Key secret = MacProvider.generateKey(SignatureAlgorithm.HS256);
    private static final byte[] secretBytes = secret.getBytes();
    private static final String base64SecretBytes = Base64.getEncoder().encodeToString(secretBytes);

    @Value("${app.authorizationCodePeriod}")
    private long authorizationCodePeriod;

    @Value("${app.accessTokenPeriod}")
    private long accessTokenPeriod;

    public String generateAuthorationCode(String id) {
        Date now = new Date();
        Date exp = new Date(System.currentTimeMillis() + (authorizationCodePeriod)); //day

        String token = Jwts.builder()
                .setHeaderParam("typ", "JWT")
                .setIssuedAt(new Date())
                .setExpiration(exp)
                .setId(id)
                .claim("CreatedDate", new Date().toString())
                .claim("type", 1)
                .signWith(SignatureAlgorithm.HS256, base64SecretBytes)
                .compact();

        return token;
    }

    public String generateAccessToken(String id) {
        Date now = new Date();
        Date exp = new Date(System.currentTimeMillis() + (accessTokenPeriod)); //day

        String token = Jwts.builder()
                .setHeaderParam("typ", "JWT")
                .setIssuedAt(new Date())
                .setExpiration(exp)
                .setId(id)
                .claim("type", 2)
                .signWith(SignatureAlgorithm.HS256, base64SecretBytes)
                .compact();

        return token;
    }

    public String generateRefreshToken(String id) {
        String token = Jwts.builder()
                .setHeaderParam("typ", "JWT")
                .setIssuedAt(new Date())
                .setId(id)
                .claim("type", 3)
                .signWith(SignatureAlgorithm.HS256, base64SecretBytes)
                .compact();

        return token;
    }

    public int getTokenType(String token){
        Claims claims = Jwts.parser()
                .setSigningKey(base64SecretBytes)
                .parseClaimsJws(token).getBody();
        return (int) claims.get("type");
    }

    public long verifyAuthorazationCode(String token) {
        try {
            Claims claims = Jwts.parser()
                    .setSigningKey(base64SecretBytes)
                    .parseClaimsJws(token).getBody();
            if (claims.getExpiration().before(new Date()))       //check expiration
                return -1;
            else if (getTokenType(token) != 1)
                return -2;
            else
                return Long.parseLong(claims.getId());
        }
        catch (Exception e){
            return -3;
        }
    }

    public long verifyAccessToken(String token) {
        try {
            Claims claims = Jwts.parser()
                    .setSigningKey(base64SecretBytes)
                    .parseClaimsJws(token).getBody();
            if (claims.getExpiration().before(new Date()))       //check expiration
                return -1;
            else if (getTokenType(token) != 2)
                return -2;
            else
                return Long.parseLong(claims.getId());
        }
        catch (Exception e){
            return -3;
        }
    }

    public long verifyRefreshToken(String token){
        try {
            Claims claims = Jwts.parser()
                    .setSigningKey(base64SecretBytes)
                    .parseClaimsJws(token).getBody();
            if (getTokenType(token) != 3)
                return -2;
            else
                return Long.parseLong(claims.getId());
        }
        catch (Exception e){
            return -3;
        }
    }

    public long getIDfromToken(String token){
        Claims claims = Jwts.parser()
                .setSigningKey(base64SecretBytes)
                .parseClaimsJws(token).getBody();
        return Long.parseLong(claims.getId());
    }

    public TokenResponse processAuthorationCode(String code){

        if(verifyAuthorazationCode(code) > 0) {
            return new AccessTokenResponse(generateAccessToken(getIDfromToken(code)+""),
                    generateRefreshToken(getIDfromToken(code)+""), accessTokenPeriod);
        }
        else{
           return new InvalidGrantTokenResponse();
        }
    }

    public TokenResponse processRefreshToken(String refreshToken){
        if(verifyRefreshToken(refreshToken) > 0 && getTokenType(refreshToken) == 3) {
            return new RefreshTokenResponse(generateAccessToken(getIDfromToken(refreshToken)+""), accessTokenPeriod);
        }
        else{
            return new InvalidGrantTokenResponse();
        }
    }
}