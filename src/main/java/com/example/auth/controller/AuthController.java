package com.example.auth.controller;

import com.example.auth.payload.login_request.LoginRequest;
import com.example.auth.payload.login_request.LoginResponse;
import com.example.auth.payload.token_request.InvalidGrantTokenResponse;
import com.example.auth.payload.token_request.TokenResponse;
import com.example.auth.repository.RoleRepository;
import com.example.auth.repository.UserLoginRepository;
import com.example.auth.security.UserPrincipal;
import com.example.auth.security.helper.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.naming.AuthenticationException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@Controller
@RequestMapping("/oauth/v2")
public class AuthController {
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserLoginRepository userLoginRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    JwtTokenProvider tokenProvider;

    @RequestMapping(value = "auth", method = RequestMethod.GET)
    public String auth(HttpServletRequest request   , Model model) throws AuthenticationException {
        String client_id = request.getParameter("client_id");
        String redirect_uri = request.getParameter("redirect_uri");
        String response_type = request.getParameter("response_type");
        String scope = request.getParameter("scope");
        String state = request.getParameter("state");

        model.addAttribute("client_id", client_id);
        model.addAttribute("redirect_uri", redirect_uri);
        model.addAttribute("response_type", response_type);
        model.addAttribute("scope", scope);
        model.addAttribute("state", state);

        return "login";
    }

    @RequestMapping(value = "login", method = RequestMethod.POST)
    public ResponseEntity<?> login(@Valid @RequestBody LoginRequest loginRequest) {
        System.out.println(loginRequest.toString());

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getEmail(),
                        loginRequest.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);
        UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
        String authorizationCode = tokenProvider.generateAuthorationCode(userPrincipal.getId() + "");
        String redirectURI = loginRequest.getRedirect_uri();
        redirectURI = redirectURI + "?code=" + authorizationCode + "&state=" + loginRequest.getState();


        return ResponseEntity.ok(new LoginResponse(true, redirectURI));
    }

    @RequestMapping(value = "token", method = RequestMethod.POST)
    public ResponseEntity<?> token(HttpServletRequest request, HttpServletResponse response) {
        String client_id = request.getParameter("client_id");
        String client_secret = request.getParameter("client_secret");
        String grant_type = request.getParameter("grant_type");
        String code = request.getParameter("code");

        TokenResponse tokenResponse = null;
        if(grant_type.equals("authorization_code"))
            tokenResponse = tokenProvider.processAuthorationCode(code);
        else if(grant_type.equals("refresh_token")) {
            String refreshToken = request.getParameter("refresh_token");
            tokenResponse = tokenProvider.processRefreshToken(refreshToken);
        }
        else{
            response.setStatus(400);
            tokenResponse = new InvalidGrantTokenResponse();
        }

        return ResponseEntity.ok(tokenResponse);
    }




}
