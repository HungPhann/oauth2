package com.example.auth.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SecurityTestController {
    @GetMapping(value = "/test")
    public ResponseEntity<?> test(){
        return ResponseEntity.ok("{success: true}");
    }
}
