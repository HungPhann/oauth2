package com.example.auth.controller;

import com.example.auth.exception.AppException;
import com.example.auth.model.Role;
import com.example.auth.model.RoleName;
import com.example.auth.model.UserLogin;
import com.example.auth.repository.RoleRepository;
import com.example.auth.repository.UserLoginRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;

@RestController
public class CreateDefaultUserController {

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private UserLoginRepository userLoginRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @GetMapping(value = "/api/create-default-user")
    public ResponseEntity createDefaultUser(){
        UserLogin user = new UserLogin("a", "phanhuyhung1707@gmail.com", "a");
        Role userRole = roleRepository.findByName(RoleName.ROLE_USER)
                .orElseThrow(() -> new AppException("User Role not set."));
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setRoles(Collections.singleton(userRole));

        UserLogin result = userLoginRepository.save(user);
        return new ResponseEntity(result, HttpStatus.OK);
    }

}
